import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "TechnoExpress",
		content: "Quality and Reliable Tech products nearest you!",
		destination: "/products",
		label: "Explore Now!"
	}


	return (
		<>
		<Banner data={data} />
    	<Highlights />
    	
		</>
	)
}


